#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "integer.h"
void initInteger(Integer *a){
	a -> p = NULL;
	a -> q = NULL;
}
void addDigit(Integer *a, char c){
	if ((isdigit(c - '0')) && ((c -'0') > 0)) { 
		int len;
		node *x, *tmp;
		len = length(*a);
	
		x = (node *)malloc(sizeof(node));
		x -> str = c;
		if(len == 0) {
			a -> p = a -> q = x;
			x -> prev = x -> next = NULL;
			return;
		}
		tmp = a -> q;
	
		tmp -> next = x;
		x -> next = NULL;
		x -> prev = tmp;
	
		a -> q = x;
	}
	else 
		printf("Please enter a valid number\n");
}

Integer createIntegerFromString(char *str){
	int i = 0;
	char c;
	Integer a;
	initInteger(&a);
	while ((isdigit(str[i] - '0')) && ((str[i] -'0') > 0)) {
		str[i] = c;
		addDigit(&a, c);
		i++;
	}
	return a;
}

Integer addIntegers(Integer a, Integer b) {
	Integer c;
	initInteger(&c);
	int d;
	char e;
	node *tmp1, *tmp2;
	tmp1 = a.q;
	tmp2 = b.q;
	while(1) {
		if (!tmp1 && tmp2) {
			tmp1 = (node *)malloc(sizeof(node));
			tmp1 -> str = 0;
		}
		else if (tmp1 && !tmp2) {
			tmp2 = (node *)malloc(sizeof(node));
			tmp2 -> str = 0;
		}
		else if (!tmp1 && !tmp2)
			break;
		d = (((tmp1 -> str) - '0') + ((tmp2 -> str) - '0'));
		e = d + '0';
		addDigit(&c, e);
		tmp1 = tmp1 -> prev;
		tmp2 = tmp2 -> prev;	
	}
	reverse(&c);
	return c;
}

Integer substractIntegers(Integer a, Integer b){
	Integer c;
	initInteger(&c);
	int d, len1, len2;
	char e;
	len1 = length(a);
	len2 = length(b);
	node *tmp1, *tmp2;
	if (len1 > len2) {
		tmp1 = a.q;
		tmp2 = b.q;
	}
	else {
		e = '0';
		addDigit(&c, e);
		return c;
	}
	while(1) {
		if (tmp1 && !tmp2) {
			tmp2 = (node *)malloc(sizeof(node));
			tmp2 -> str = '0';
		}
		else if (!tmp1 && !tmp2)
			break;
		d = (((tmp1 -> str) - '0') - ((tmp2 -> str) - '0'));
		e = d + '0';
		addDigit(&c, e);
		tmp1 = tmp1 -> prev;
		tmp2 = tmp2 -> prev;	
	}
	reverse(&c);
	return c;

}


void printInteger(Integer a){
	node *tmp;
	tmp = a.p;
	if(!tmp) {
		printf("\n");
		return;
	}
	do {
		printf("%c", tmp -> str);
		tmp = tmp -> next;
	}while(tmp != a.p);
	printf("\n");
}

void destroyInteger(Integer *a) {
	if (a -> p == NULL)
		printf("List is empty\n");
	else {
		node *tmp;
		int i, len = length(*a);
		for (i = 0; i < len; i++) {
			tmp = a -> p;
			if (tmp -> prev == tmp -> next) {
				a-> p == NULL;
				free(tmp);
			}
			else {
				a -> p = tmp -> next;
				a -> p -> prev = NULL;
				free(tmp);
			}
		}
	}
}

void reverse(Integer *a){
	node *p, *q, *r, *tmp;
	if(a -> p == NULL)
		return;
	q = a -> p;
	p = q -> next;
	r = p -> next;
	while(r != q) {
		p -> next = q;
		q = p;
		p = r;
		r = r -> next;
	}

	q = a -> q;
	p = q -> prev;
	r = p -> prev;
	while(r != q) {
		p -> prev = q;
		q = p;
		p = r;
		r = r -> prev;
	}
	
	tmp = a -> p;
	a -> p = a -> q;
	a -> q = tmp;
}

int length(Integer a) {
	int len = 0;
	node *tmp;
	tmp = a.p;
	if(!tmp)
		return 0;
	do {
		tmp = tmp -> next;
		len++;
	}while(tmp != NULL);
	len++;
	return len;
}
